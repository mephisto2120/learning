package com.tryton.learning.brickshop_mongodb.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.tryton.learning.brickshop_mongodb.model.Order;

public class OrderToOrderDtoConverterTest {

    private final OrderToOrderDtoConverter orderConverter = new OrderToOrderDtoConverter();

    @Test
    public void shouldConvertOrders() {
        // given
        int bricksCount1 = 100;
        String orderReferenceId1 = "123abc";
        Order first = new Order(bricksCount1, orderReferenceId1);

        int bricksCount2 = 200;
        String orderReferenceId2 = "123def";
        Order second = new Order(bricksCount2, orderReferenceId2);

        OrderDTO firstExpected = new OrderDTO(orderReferenceId1, bricksCount1);
        OrderDTO secondExpected = new OrderDTO(orderReferenceId2, bricksCount2);

        // when
        List<OrderDTO> result = orderConverter.convert(Arrays.asList(first, second));

        // then
        assertThat(result).containsExactly(firstExpected, secondExpected);
    }
}