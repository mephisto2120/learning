package com.tryton.learning.brickshop_mongodb.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.tryton.learning.brickshop_mongodb.service.OrderService;



@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OrderCheckerControllerTest {
  private static final String ORDER_DISPATCHED = "The Order is marked as dispatched";
  private static final String ORDER_NOT_DISPATCHED = "The Order is not marked as dispatched";
  private static final String JOHN = "john";
  
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private OrderService orderService;
  
  @Test
  public void shouldCheckWhetherOrdersWereDispatched() throws Exception {
    // given
    MvcResult result1 = performCreateOrder(JOHN, 100);
    MvcResult result2 = performCreateOrder(JOHN, 200);
    
    String orderReferenceId1 = result1.getResponse().getContentAsString();
    String orderReferenceId2 = result2.getResponse().getContentAsString();
    markAsDispatched(orderReferenceId2);
    
    // when - then
    mockMvc.perform(get("/ordercheck/" + orderReferenceId1)).andDo(print()).andExpect(status().isOk())
      .andExpect(content().string(ORDER_NOT_DISPATCHED));
    mockMvc.perform(get("/ordercheck/" + orderReferenceId2)).andDo(print()).andExpect(status().isOk())
      .andExpect(content().string(ORDER_DISPATCHED));
  }
  
  private MvcResult performCreateOrder(String user, int content) throws Exception {
    return mockMvc.perform(post("/orders/" + user).content(Integer.toString(content)))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$", notNullValue()))
      .andReturn();
  }
  
  private void markAsDispatched(String orderReferenceId) {
    orderService.updateOrder(orderReferenceId, true);
  }
  
  @Test
  public void shouldReturnBadRequestForNonexistentReferenceId() throws Exception {
    // given-when-then
    mockMvc.perform(get("/ordercheck/nonexistentReferenceId")).andExpect(status().isBadRequest());
  }
}