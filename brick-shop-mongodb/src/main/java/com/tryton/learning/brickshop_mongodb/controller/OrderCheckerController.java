package com.tryton.learning.brickshop_mongodb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tryton.learning.brickshop_mongodb.service.OrderNotFoundException;
import com.tryton.learning.brickshop_mongodb.service.OrderService;

@RestController
@RequestMapping("/ordercheck")
@RequiredArgsConstructor
public class OrderCheckerController {
    private static final String ORDER_DISPATCHED = "The Order is marked as dispatched";
    private static final String ORDER_NOT_DISPATCHED = "The Order is not marked as dispatched";

    @Autowired
    private final OrderService orderService;

    @RequestMapping(value = "/{referenceId}", method = RequestMethod.GET, produces = "application/json")
    public String isOrderDispatched(@PathVariable String referenceId) {
        return orderService.isOrderDispatched(referenceId) ? ORDER_DISPATCHED : ORDER_NOT_DISPATCHED;
    }

    @ExceptionHandler(OrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity orderDoesNotExist() {
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.BAD_REQUEST);
    }
}
