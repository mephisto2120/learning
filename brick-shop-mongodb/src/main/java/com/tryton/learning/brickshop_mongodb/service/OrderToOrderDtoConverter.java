package com.tryton.learning.brickshop_mongodb.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.tryton.learning.brickshop_mongodb.model.Order;

@Component
class OrderToOrderDtoConverter {

    public List<OrderDTO> convert(List<Order> orders) {
        return orders.stream()
                .map(OrderToOrderDtoConverter::convert)
                .collect(Collectors.toList());
    }

    private static OrderDTO convert(Order order) {
        return new OrderDTO(order.getReferenceId(), order.getBrickCount());
    }
}
