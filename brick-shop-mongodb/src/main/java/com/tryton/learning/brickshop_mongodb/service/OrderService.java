package com.tryton.learning.brickshop_mongodb.service;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tryton.learning.brickshop_mongodb.model.Customer;
import com.tryton.learning.brickshop_mongodb.model.Order;
import com.tryton.learning.brickshop_mongodb.repository.CustomerRepository;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderService {
    @Autowired
    private final CustomerRepository customerRepository;
    @Autowired
    private final OrderToOrderDtoConverter orderConverter;

    public String createOrder(String customerName, int brickCount) {
        Order order = new Order(brickCount);
        Customer customer = getOrCreateCustomer(customerName, order);
        customerRepository.save(customer);
        return order.getReferenceId();
    }

    private Customer getOrCreateCustomer(String customerName, Order order) {
        return Optional.ofNullable(customerRepository.findByName(customerName))
                .map(foundCustomer -> updateCustomerOrders(foundCustomer, order))
                .orElse(createCustomer(customerName, order));
    }

    private static Customer createCustomer(String customerName, Order order) {
        return Customer.builder()
                .name(customerName)
                .orders(Collections.singletonList(order))
                .build();
    }

    private static Customer updateCustomerOrders(Customer foundCustomer, Order order) {
        List<Order> orders = createOrders(foundCustomer.getOrders(), order);
        return createCustomer(foundCustomer, orders);
    }

    private static Customer createCustomer(Customer foundCustomer, List<Order> orders) {
        return Customer.builder()
                .id(foundCustomer.getId())
                .name(foundCustomer.getName())
                .orders(orders)
                .build();
    }

    private static List<Order> createOrders(List<Order> orders, Order order) {
        List<Order> newOrders = new ArrayList<>(orders);
        newOrders.add(order);
        return newOrders;
    }

    public List<OrderDTO> getCustomerOrders(String customerName) {
        return Optional.ofNullable(customerRepository.findByName(customerName))
                .map(customer -> orderConverter.convert(customer.getOrders()))
                .orElseThrow(() -> new CustomerNotFoundException(customerName));
    }

    public String updateOrder(String referenceId, int newBrickCount) {
        Customer foundCustomer = customerRepository.findByReferenceId(referenceId);
        Optional<Order> foundOrder = findOrder(foundCustomer, referenceId);
        foundOrder.ifPresent(order -> updateOrder(foundCustomer, order, referenceId, newBrickCount));
        return referenceId;
    }
    
    private void updateOrder(Customer foundCustomer, Order order, String referenceId, int newBrickCount) {
        if (order.isDispatched()) {
            throw new OrderDispatchedException(referenceId);
        }
        Order updatedOrder = new Order(newBrickCount, referenceId);
        updateOrder(foundCustomer, referenceId, updatedOrder);
    }
    
    public String updateOrder(String referenceId, boolean dispatched) {
        Customer foundCustomer = customerRepository.findByReferenceId(referenceId);
        Optional<Order> foundOrder = findOrder(foundCustomer, referenceId);
        if (foundOrder.isPresent()) {
            Order updatedOrder = new Order(foundOrder.get().getBrickCount(), new Date().getTime(),
                    foundOrder.get().getReferenceId(), dispatched);
            updateOrder(foundCustomer, referenceId, updatedOrder);
        }
        return referenceId;
    }

    private static Optional<Order> findOrder(Customer foundCustomer, String referenceId) {
        return foundCustomer.getOrders().stream()
                .filter(order -> hasEqualReferenceId(referenceId, order))
                .findFirst();
    }

    private static boolean hasEqualReferenceId(String referenceId, Order order) {
        return order.getReferenceId().equals(referenceId);
    }

    private void updateOrder(Customer foundCustomer, String referenceId, Order updatedOrder) {
        List<Order> filteredOrders = filterOutUpdatedOrder(referenceId, foundCustomer);
        List<Order> orders = createOrders(filteredOrders, updatedOrder);
        Customer updatedCustomer = createCustomer(foundCustomer, orders);
        customerRepository.save(updatedCustomer);
    }

    private List<Order> filterOutUpdatedOrder(String referenceId, Customer foundCustomer) {
        return foundCustomer.getOrders().stream()
                .filter(order -> !hasEqualReferenceId(referenceId, order))
                .collect(toList());
    }

    public boolean isOrderDispatched(String referenceId) {
        return Optional.ofNullable(customerRepository.findByReferenceId(referenceId))
                .map(foundCustomer -> isOrderDispatched(foundCustomer, referenceId))
                .orElseThrow(() -> new OrderNotFoundException(referenceId));
    }

    private boolean isOrderDispatched(Customer foundCustomer, String referenceId) {
        Optional<Order> foundOrder = findOrder(foundCustomer, referenceId);
        return foundOrder.isPresent() && foundOrder.get().isDispatched();
    }
}
