package com.tryton.learning.brickshop_mongodb.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
class EmptyJsonResponse {
}