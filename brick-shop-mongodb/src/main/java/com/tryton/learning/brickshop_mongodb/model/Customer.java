package com.tryton.learning.brickshop_mongodb.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document(collection="customer")
@Getter
@Builder
public class Customer {
    @Id
    private BigInteger id;
    private final String name;
    private List<Order> orders = new ArrayList<>();
}