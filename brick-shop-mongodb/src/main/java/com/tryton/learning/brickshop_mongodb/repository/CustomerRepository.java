package com.tryton.learning.brickshop_mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tryton.learning.brickshop_mongodb.model.Customer;

public interface CustomerRepository extends MongoRepository<Customer, Long> {
    Customer findByName(String name);

    @Query("{'orders.referenceId': ?0}")
    Customer findByReferenceId(String referenceId);
}