package com.tryton.learning.brickshop_mongodb.model;

import java.util.Date;
import java.util.UUID;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Order {
    private final int brickCount;
    private final long creationDateTimestamp;
    private final String referenceId;
    private final boolean dispatched;

    public Order() {
        this(0, new Date().getTime(), UUID.randomUUID().toString(), false);
    }

    public Order(int brickCount) {
        this(brickCount, new Date().getTime(), UUID.randomUUID().toString(), false);
    }

    public Order(int brickCount, String referenceId) {
        this(brickCount, new Date().getTime(), referenceId, false);
    }
}