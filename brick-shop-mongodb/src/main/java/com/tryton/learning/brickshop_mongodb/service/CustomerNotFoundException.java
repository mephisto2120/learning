package com.tryton.learning.brickshop_mongodb.service;

public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException(String username) {
        super(String.format("Customer %s not found", username));
    }
}