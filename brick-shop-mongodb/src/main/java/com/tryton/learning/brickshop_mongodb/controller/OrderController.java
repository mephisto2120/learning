package com.tryton.learning.brickshop_mongodb.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tryton.learning.brickshop_mongodb.service.CustomerNotFoundException;
import com.tryton.learning.brickshop_mongodb.service.OrderDTO;
import com.tryton.learning.brickshop_mongodb.service.OrderDispatchedException;
import com.tryton.learning.brickshop_mongodb.service.OrderService;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {
    @Autowired
    private final OrderService orderService;

    @RequestMapping(value = "/{username}", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String createOrder(@PathVariable String username, @RequestBody String content) {
        return orderService.createOrder(username, Integer.parseInt(content));
    }

    @RequestMapping(value = "/{referenceId}", method = RequestMethod.PUT, produces = "application/json")
    public String updateOrder(@PathVariable String referenceId, @RequestBody String content) {
        return orderService.updateOrder(referenceId, Integer.parseInt(content));
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = "application/json")
    public List<OrderDTO> getCustomerOrders(@PathVariable String username) {
        return orderService.getCustomerOrders(username);
    }

    @ExceptionHandler(CustomerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity customerDoesNotExist() {
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @ExceptionHandler(OrderDispatchedException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity orderDispatched() {
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.BAD_REQUEST);
    }
}