package com.tryton.learning.brickshop_mongodb.service;

public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(String referenceId) {
        super(String.format("Order with referenceId: %s not found", referenceId));
    }
}