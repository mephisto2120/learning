package com.tryton.learning.brickshop_mongodb.service;

public class OrderDispatchedException extends RuntimeException {
    public OrderDispatchedException(String referenceId) {
        super(String.format("Order with referenceId: %s was dispatched", referenceId));
    }
}